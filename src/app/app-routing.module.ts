import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {InfiniteTableComponent} from './infinite-table/infinite-table.component';

const routes: Routes = [
  {component: InfiniteTableComponent, path: 'infinite'},
  {component: HomeComponent, path: '', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
