import {Component, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {map, take} from 'rxjs/operators';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

  page$ = this.data.metadata$.pipe(map(meta => meta.page));
  size$ = this.data.metadata$.pipe(map(meta => meta.size));
  total$ = this.data.metadata$.pipe(map(meta => meta.total));

  size = 10;

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.goTo(1, this.size);
  }

  goToPage(page: number) {
    this.size = Math.max(this.size, 0);
    this.data.goTo(page, this.size);
  }

  goToNext() {
    this.page$.pipe(take(1)).subscribe(page => this.data.goTo(page + 1, this.size));
  }

  goToPrevious() {
    this.page$.pipe(take(1)).subscribe(page => this.data.goTo(page - 1, this.size));
  }

  sizeChanged() {
    if (this.size < 1) {
      this.size = 10;
    }
    this.goToPage(1);
  }

}
