import {Injectable} from '@angular/core';
import {filter, take} from 'rxjs/operators';
import {Datum} from './data.service';
import {BehaviorSubject, ReplaySubject} from 'rxjs';
import {sample} from './data/sample_data';

@Injectable({
  providedIn: 'root'
})
export class ScrollService {

  private scrollBuffer = 50;
  private scrollIncrement = 10;

  private store$ = new ReplaySubject<Datum[]>(1);
  private dataStream$ = this.store$.pipe(filter(res => !!res));
  public results$ = this.dataStream$;

  private firstItem = sample[0];
  private lastItem = sample[this.scrollBuffer - 1];

  constructor() {
    this.store$.next(sample.slice(0, this.scrollBuffer));
  }

  scrollNext() {
    const startIndex = sample.indexOf(this.lastItem) + 1;
    this.results$.pipe(take(1)).subscribe(results => {
      const newResults = results.concat(sample.slice(startIndex, startIndex + this.scrollIncrement)).slice(-50);
      this.store$.next(newResults);
      this.firstItem = newResults[0];
      this.lastItem = newResults[this.scrollBuffer - 1];
    });
  }

  scrollPrevious() {
    const endIndex = sample.indexOf(this.firstItem);
    let slice: Datum[];
    if (endIndex - this.scrollIncrement > 0) {
      slice = sample.slice(endIndex - this.scrollIncrement, endIndex);
    } else if (endIndex > 0) {
      slice = sample.slice(0, endIndex);
    } else {
      return true;
    }
    this.results$.pipe(take(1)).subscribe(results => {
      const newResults = slice.concat(results).slice(0, 50);
      this.store$.next(newResults);
      this.firstItem = newResults[0];
      this.lastItem = newResults[this.scrollBuffer - 1];
    });
    return false;
  }
}
