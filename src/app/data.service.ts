import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {sample} from './data/sample_data';
import {filter, map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private store$ = new ReplaySubject<Response>(1);
  private dataStream$ = this.store$.pipe(filter(res => !!res));
  public results$ = this.dataStream$.pipe(map(res => res.data));
  public metadata$ = this.dataStream$.pipe(map(res => res.metadata));

  constructor(private http: HttpClient) { }

  goTo(page: number, size: number) {
    const zeroBasedPage = page - 1;
    if (zeroBasedPage * size >= sample.length) {
      const offsetFromEnd = sample.length % size || size;
      const startingIndex = sample.length - offsetFromEnd;
      this.store$.next({
        data: sample.slice(startingIndex),
        metadata: {
          page,
          size,
          total: sample.length
        }
      });
    } else {
      const startingIndex = zeroBasedPage * size;
      this.store$.next({
        data: sample.slice(startingIndex, startingIndex + size),
        metadata: {
          page,
          size,
          total: sample.length
        }
      });
    }
  }

  post(id: number, status: string) {
    this.http.post('/api/submit', {id, status}).subscribe(console.dir, console.error, console.dir);
  }
}

export interface Response {
  data: Datum[];
  metadata: {
    page: number,
    size: number,
    total: number
  };
}

export interface Datum {
  name: string;
  phone: string;
  email: string;
  company: string;
  date_entry: string;
  org_num: string;
  address_1: string;
  city: string;
  zip: string;
  geo: string;
  pan: string;
  pin: string;
  id: number;
  status: string;
  fee: string;
  guid: string;
  date_exit: string;
  date_first: string;
  date_recent: string;
  url: string;
}
