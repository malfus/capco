import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.sass']
})
export class PaginationComponent {

  @Input() page: number;
  @Input() count: number;
  @Input() perPage: number;
  @Input() loading: boolean;
  @Input() pagesToShow = 9;

  @Output() goPrev = new EventEmitter<boolean>();
  @Output() goNext = new EventEmitter<boolean>();
  @Output() goPage = new EventEmitter<number>();

  constructor() { }

  onPage(n: number): void {
    this.goPage.emit(n);
  }

  onPrev(): void {
    this.goPrev.emit(true);
  }

  onNext(next: boolean): void {
    this.goNext.emit(next);
  }

  lastPage(): boolean {
    return this.perPage * this.page >= this.count;
  }

  getPages(): number[] {
    const maxPage = Math.ceil(this.count / this.perPage);
    const currentPage = this.page || 1;
    const pagesToShow = this.pagesToShow;
    const pages: number[] = [];
    const startPage = Math.max(1, currentPage - Math.floor(pagesToShow / 2));
    for (let i = 0; i < pagesToShow; i++) {
      if (startPage + i <= maxPage) {
        pages.push(startPage + i);
      }
    }
    return pages;
  }

}
