import {Component, HostListener, OnInit} from '@angular/core';
import {ScrollService} from '../scroll.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-infinite-table',
  templateUrl: './infinite-table.component.html',
  styleUrls: ['./infinite-table.component.sass']
})
export class InfiniteTableComponent implements OnInit {

  results$ = this.scrollService.results$;
  headers$ = this.results$.pipe(map(results => results[0]), map(datum => Object.keys(datum)));

  constructor(private scrollService: ScrollService) { }

  ngOnInit() {
  }

  @HostListener('window:scroll', ['$event'])
  onScroll($event: Event) {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.scrollService.scrollNext();
    } else if (window.scrollY === 0) {
      const isFirstItem = this.scrollService.scrollPrevious();
      if (!isFirstItem) {
        window.scrollBy(0, 275);
      }
    }
  }
}
