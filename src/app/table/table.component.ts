import {Component, HostListener} from '@angular/core';
import {DataService} from '../data.service';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass']
})
export class TableComponent {


  headers$ = this.data.results$.pipe(map(results => results[0]), map(sample => Object.keys(sample)));
  results$ = this.data.results$;

  constructor(private data: DataService) { }

  submit(id: number, status: string) {
    this.data.post(id, status);
  }
}
